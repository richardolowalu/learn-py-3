#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

def main():
    print('Hello, World.')
    s = 1
    j = 2
    if s == 1: print(f"j = {j}")

if __name__ == '__main__': main()
