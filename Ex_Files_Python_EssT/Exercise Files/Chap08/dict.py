#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

def main():
    animals = { 'kitten': 'meow', 'puppy': 'ruff!', 'lion': 'grrr',
        'giraffe': 'I am a giraffe!', 'dragon': 'rawr' }

    people = { "john": "hi", "dave": "bye"}

    print_dict(animals)
    p_d2(people)

def p_d2(o):
    for x in o:
        print(f"{x} says {o[x]}")

def print_dict(o):
    for x in o: print(f'{x}: {o[x]}')

if __name__ == '__main__': main()
