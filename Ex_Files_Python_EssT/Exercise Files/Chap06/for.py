#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

animals = ( 'bear', 'bunny', 'dog', 'cat', 'velociraptor' )

for pet in animals:
    print(pet)

## https://www.geeksforgeeks.org/python-convert-string-tuples-to-list-tuples/?ref=rp

# Initializing list  
test_list = ["('gfg', 1)", "('is', 2)", "('best', 3)"] 
  
# printing original list  
print("The original list is : " + str(test_list)) 
  
# Converting string tuples to list tuples  
# using list comprehension + eval() 
res = [eval(ele) for ele in test_list] 
  
# printing result 
print("The list tuple after conversion : " + str(res)) 
