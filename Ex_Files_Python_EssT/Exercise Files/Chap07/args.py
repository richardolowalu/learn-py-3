#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

def main2():
    kitten('meow', 'grrr', 'purr')

def kitten(a,*args):
    print('this is a {}'.format(a))
    if len(args):
        for s in args:
            print(s)
    else: print('Meow.')

def main():
    print('cat 1')
    cat("woaa")
    print('cat 2')
    cat('jo',"ko",'lo')

def cat(b,*args):
    print(f'this is a {b}')
    if len(args):
        for s in args:
            print(s)
    else:
        print('cat done')

if __name__ == '__main__': main()
