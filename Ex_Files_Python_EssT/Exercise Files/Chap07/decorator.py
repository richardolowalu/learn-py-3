#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

# https://www.programiz.com/python-programming/decorator
import time

def elapsed_time(f):
    def wrapper(a):
        t1 = time.time()
        f(a)
        t2 = time.time()
        print(f'Elapsed time: {(t2 - t1) * 1000} ms')
    return wrapper


@elapsed_time
def big_sum(aa):
    #a = 1
    print(f"this is aa {aa}")
    num_list = []
    for num in (range(0, 100)):
        num_list.append(num)
    print(f'Big sum: {sum(num_list)}')

def ff(*args, **kwargs):
    print(args)
    print(kwargs)

def main():
    big_sum("this is aa")

    ff('1', '2', a=1, b=2)
    zz = ('1', '2')
    yy = {'a': 1, 'b': 2}
    ff(*zz, **yy)

if __name__ == '__main__': main()
