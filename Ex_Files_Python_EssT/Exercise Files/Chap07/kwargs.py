#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/
my = dict(jj = 1, kk = 2, ll = 3)
for k in my:
    print("this is {} it {}".format(k,my[k]))
    print(f"this is {k} it {my[k]}")

def main():
    kitten(Buffy = 'meow', Zilla = 'grr', Angel = 'rawr')

def kitten(**kwargs):
    if len(kwargs):
        for k in kwargs:
            print('Kitten {} says {}'.format(k, kwargs[k]))
    else: print('Meow.')

if __name__ == '__main__': main()
