#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

x = [ 1, 2, [3,33,333], 4, 5 ]
for i in x:
    print('i is {}'.format(i))
    if isinstance(i, list):
        for x in i:
            print('x is {}'.format(x))
