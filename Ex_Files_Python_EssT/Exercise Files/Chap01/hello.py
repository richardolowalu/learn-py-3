#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

print('Hello, World.')

x = 1
def hi():
    print('hiya {}'.format(x))
    return('hi')
hi()

class myClass():
    def myFunct(self):
        print('in myFunct')
        
        
def main():
    myc = myClass()
    myc.myFunct()
    
if __name__ == '__main__': main()
