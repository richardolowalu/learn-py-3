#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

x = 42
y = 73

if x < y:
    print('x < y: x is {} and y is {}'.format(x, y))
    print('this is "subst {} and again {}'.format("choto","mo"))
    print("this is 'subst {}".format(x))
print('heee h',x)

# python string

s = "this is a string\nand it is still a string"
print(s)

s2 = """\
hiya
bya\
"""

s3 = 'done'

print(s)
print(s2)
print(s3)

tx = ( "this is " "something " "more")
print(tx)

ty = tx + tx
print(ty)

word = "mywords"


word2 = word[:2]
print(word2)

word3 = word[2:]

print(word3)

# list  https://docs.python.org/3/tutorial/interpreter.html
# 3.1.3. Lists

l = [ 1, 2 , 3, "rere sesf", 5]
print(l)
print(l[3])
print(l[2:])
l[2]=22
print(l)
l.append("jojo")
print(l)
print(len(l))

a, b = 0, 1
while a < 10:
   print(a,end="")
   a, b = b, a+b

#python list
print('done')

#"starting" - 4. More Control Flow Tools
