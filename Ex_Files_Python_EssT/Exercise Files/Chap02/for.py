#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

words = ['one', 'two', 'three', 'four', 'five']

for i in words:
    print(i)

thislist = ["apple", "banana", "cherry"]
for x in thislist:
  print(x)

mylist = ['a', 'b', 'c']
mylist[2] = 'd'

if 'd' in mylist:
    print("d is in mylist")

for y in mylist:
    print(y)

len = len(mylist)
print('what is the len of my list? it is -{}-'.format(len))

mylist.insert(3,"e")
print(mylist)

xx = 1
yy = 2

zz = yy

yy = 3

print('zz 2 or 3 {}'.format(zz))

a = ['1','2']
b = ['3','4']

b = a
a[0] = '11'

c = b[:]
print(b)
b[0] = '44'
print(c)
print(b)
print(id(b))
print(id(c))
print(id(zz))
print(id(yy))
