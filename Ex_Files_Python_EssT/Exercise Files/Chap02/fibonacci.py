#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

# simple fibonacci series
# the sum of two elements defines the next set

a, b = 0, 1
c = 3
while b < 1000:
    print("----------")
    print(b, end = ' ', flush = True)
    print('a {} b {} c {}'.format(a,b,c))
    a, b, c = b, a + b, c + c
    print('a {} b {} c {}'.format(a,b,c))

print() # line ending

# 1
# 1
# 
