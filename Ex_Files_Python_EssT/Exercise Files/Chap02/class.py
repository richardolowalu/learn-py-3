#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

class Duck:
    def quack(self):
        print('Quaaack!')

    def walk(self):
        print('Walks like a duck.')


class Dog:
    def bark(self):
        print('woof woof')
    
    def run(self):
        print("running free")

def main():
    donald = Duck()
    donald.quack()
    donald.walk()

    gretchen = Dog()
    gretchen.bark()
    gretchen.run()

if __name__ == '__main__': main()


