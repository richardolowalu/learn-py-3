#!/usr/bin/env python3
# Copyright 2009-2017 BHG http://bw.org/

x = 5
y = 3
z = x + y

print(f'result is {z}')

str = "\}"
print('this is y {} and x {} {}'.format(x,y,str))

a, b = 10, 20
  
# Use tuple for selecting an item 
print( (b, a) [a < b] ) 
